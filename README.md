# Python Programming Tutorial for beginners

This project includes example demos for learning basic concepts of Python and programming in general, such as:
- data types
- variables
- functions
- conditionals
- loops
- modules and packages
- classes and objects
- etc

# The project belongs to: 

- [ ] [Python Programming Course on Youtube](https://www.youtube.com/c/TechWorldwithNana)

- [ ] [Complete Educational Program for DevOps engineers](https://www.techworld-with-nana.com/devops-bootcamp)

--- 

### Demo Project:
Write Countdown Application
### Technologies used:
Python, IntelliJ, Git
### Project Description:
- Write an application that accepts a user input of a goal and a deadline (date). Print the remaining time until that deadline

--- 

### Demo Project:
API Request to GitLab
### Technologies used:
Python, GitLab, IntelliJ, Git
### Project Description:
- Write an application that talks to an API of an external application (GitLab) and lists all the public GitLab repositories for a specified user
- Download Python Request library for api call
- Pass your gitlab projects url to request.get("url") method
- List project names and their urls

---

### Demo Project:
Automation with Python
### Technologies used:
Python, IntelliJ, Git
### Project Description:
- Write an application that reads a spreadsheet file and process and manipulate the spreadsheet
- Download openpyxl library to process the spreadsheet
- Using inventory.xlsx:
 - calculate number of products per supplier
 - calculate total value of inventory per supplier
 - find products with inventory less than 10

---

Module 13: Programming with Python